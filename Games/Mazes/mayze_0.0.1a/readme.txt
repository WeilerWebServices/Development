Mayze 0.0.1a Readme

Welcome to Mayze 0.0.1a! Please note this is an alpha, and it may be unstable. The game itself is also not finished, but many aspects of it are.

Mayze--

To play game, run mayze.py. Then you must pick a level. To do so, select it by clicking it once, and then click load.

Gameplay

Arrow keys to move, and space to shoot. If you get hit by an enemy, you loose a life and become invincible for a brief moment. If you loose all your lives, a �You Loose!� message will appear on the screen, and you will then select another level. If you kill all the enemies, A �You Win!� message will appear on the screen, and you will select another level.

Level Editor--

One of the biggest parts of this project is the level editor. The level editor screen is realizable to help when creating big levels. The drop-down box lets you select a type of tile to put on the level. You can only have one tile at a time in one place. To move the level around, simply move your cursor to the edge of the screen and it will automatically scroll. Rocks are solid walls, redballs are enemies, and player is you. You can only have one player.

When you move your cursor over a spot that already has a tile on it, your cursor will become an X, and if you are in the same mode from the drop-down box as the tile your want to delete, pressing your cursor will delete that tile. When you are not in the same mode as the tile, pressing your cursor will have no effect.

The properties dialog, from the edit->properties menu, allows you to change the background color of the level as well as add some music. To add music, click on the �browse� button or type in the text box. To change the background color, click on the �background color� button. Set the color using the three sliders, and then close the window by pressing on the x. If you want to apply your changes, click on the �OK� button. Otherwise, click on the x at the top of the window or click on the �cancel� button.

To save your level, go to file->save. Type in your desired level name (levels CAN be overwritten), and press OK. To load a level, go to file->open. Select a level, then press open. To start with a blank level, go to file->new. You may change the number of rows and columns, and then press OK. All previous data will be erased.

Todo:

Finish game.

Current bugs:

May be unstable.

Version History:

0.0.1a--

Created game.