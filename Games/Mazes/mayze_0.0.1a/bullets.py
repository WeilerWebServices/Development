##This file is part of Mayze.
##
##Mayze is free software: you can redistribute it and/or modify
##it under the terms of the GNU General Public License as published by
##the Free Software Foundation, either version 3 of the License, or
##(at your option) any later version.
##
##Mayze is distributed in the hope that it will be useful,
##but WITHOUT ANY WARRANTY; without even the implied warranty of
##MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##GNU General Public License for more details.
##
##You should have received a copy of the GNU General Public License
##along with Mayze.  If not, see <http://www.gnu.org/licenses/>.
import pygame
from functions import *
global bullet_img
bullet_img = pygame.image.load(fixpath('./data/images/bullets/normal.png'))
bullet_img.set_colorkey(bullet_img.get_at((0,0)))

class Bullet(pygame.sprite.Sprite):
    """ A bullet in a game """
    def __init__(self, x, y, dx, dy, collidedict):
        pygame.sprite.Sprite.__init__(self)
        self.image = bullet_img
        self.rect = self.image.get_rect()
        self.x = x; self.y = y
        self.dx = dx; self.dy = dy
        self.startx = x; self.starty = y
        self.life = 200; self.type = 'bullet'
        self.collidedict = collidedict
        self.paused = False

    def pause(self):
        self.paused = not self.paused

    def update(self, blocks, enemies):
        if not self.paused:
            self.x += self.dx
            self.y += self.dy

            for block in blocks.sprites():
                if abs(self.x-block.x) < 40 and\
                   abs(self.y-block.y) < 40:
                    if self.rect.colliderect(block.rect):
                        self.kill()

            for enemy in enemies.sprites():
                if abs(self.x-enemy.x) < 30 and\
                   abs(self.y-enemy.y) < 30:
                    if self.rect.colliderect(enemy.rect):
                        if self.collidedict[enemy.type] == 'kill':
                            enemy.kill()
                            self.kill()

            if abs(self.x - self.startx) > self.life: self.kill()
            if abs(self.y - self.starty) > self.life: self.kill()
            self.rect.centerx = self.x
            self.rect.centery = self.y
