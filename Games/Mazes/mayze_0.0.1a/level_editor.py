##This file is part of Mayze.
##
##Mayze is free software: you can redistribute it and/or modify
##it under the terms of the GNU General Public License as published by
##the Free Software Foundation, either version 3 of the License, or
##(at your option) any later version.
##
##Mayze is distributed in the hope that it will be useful,
##but WITHOUT ANY WARRANTY; without even the implied warranty of
##MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##GNU General Public License for more details.
##
##You should have received a copy of the GNU General Public License
##along with Mayze.  If not, see <http://www.gnu.org/licenses/>.
from pygame.locals import *
from pgu import gui
from functions import *
import pygame, sys, os

class Static(pygame.sprite.Sprite):
    """ A static image """
    def __init__(self, x, y, image, value=None):
        pygame.sprite.Sprite.__init__(self)
        self.image = image
        self.rect = image.get_rect()
        self.value = value # ID
        self.x = x; self.y = y
        self.rect.x = x; self.rect.y = y # so we don't have to ever update them

    def update(self):
        self.rect.x = self.x
        self.rect.y = self.y

class Pointer(pygame.sprite.Sprite):
    """A pointer"""
    def __init__(self, x, y, curstate, states=['add', 'delete', 'move', 'up', 'down', 'left', 'right', 'upleft', 'downleft', 'downright', 'upright']):
        pygame.sprite.Sprite.__init__(self)
        self.images = {}; self.states = states
        for state in states:
            img = pygame.image.load(fixpath('./data/images/pointers/%s.png') % state)
            img.set_colorkey(img.get_at((1,0)))
            imgsize = img.get_size()
            if state == 'add': img = pygame.transform.scale(img, (imgsize[0]*2, imgsize[1]*2))
            else: img = pygame.transform.scale2x(img)
            self.images[state] = img
            del imgsize, img
        self.image = self.images[curstate]
        self.state = curstate
        self.rect = self.image.get_rect()
        self.x = x; self.y = y

    def set_state(self, state):
        if state in self.states:
            self.state = state
            self.image = self.images[state]
            self.rect = self.image.get_rect()
            self.rect.centerx = self.x; self.rect.centery = self.y
        else: raise KeyError, 'state not found'

    def set_pos(self, pos):
        self.x = pos[0]
        self.y = pos[1]

    def get_pos(self): return (self.x, self.y)

    def update(self):
        self.rect.centerx = self.x
        self.rect.centery = self.y

class ColorDialog(gui.Dialog):
    def __init__(self,value,**params):
        self.value = list(pygame.Color(value))
        
        title = gui.Label("Color Picker")
        
        main = gui.Table()
        
        main.tr()
        
        self.color = gui.Color(self.value,width=64,height=64)
        main.td(self.color,rowspan=3,colspan=1)
        
        main.td(gui.Label(' Red: '),1,0)
        e = gui.HSlider(value=self.value[0],min=0,max=255,size=32,width=128,height=16)
        e.connect(gui.CHANGE,self.adjust,(0,e))
        main.td(e,2,0)

        main.td(gui.Label(' Green: '),1,1)
        e = gui.HSlider(value=self.value[1],min=0,max=255,size=32,width=128,height=16)
        e.connect(gui.CHANGE,self.adjust,(1,e))
        main.td(e,2,1)

        main.td(gui.Label(' Blue: '),1,2)
        e = gui.HSlider(value=self.value[2],min=0,max=255,size=32,width=128,height=16)
        e.connect(gui.CHANGE,self.adjust,(2,e))
        main.td(e,2,2)
                        
        gui.Dialog.__init__(self,title,main)
        
    def adjust(self,value):
        n,e = value
        self.value[n] = e.value
        self.color.repaint()
        self.send(gui.CHANGE)

class NewDialog(gui.Dialog):
    """ The dialog for creating a new level """
    def __init__(self, value, **params):
        title = gui.Label("New Level")
        main = gui.Table()

        main.tr()
        main.td(gui.Label('Rows: '))

        self.rows = gui.Input('15', 3)
        main.td(self.rows)

        main.tr()
        main.td(gui.Label('Cols: '))

        self.cols = gui.Input('20', 3)
        main.td(self.cols)

        main.tr()
        e = gui.Button('Ok')
        e.connect(gui.CLICK, self.finish, None)
        main.td(e)
        
        e = gui.Button('Cancel')
        e.connect(gui.CLICK, self.close, None)
        main.td(e)

        gui.Dialog.__init__(self, title, main)

    def finish(self, value):
        pygame.event.post(pygame.event.Event(USEREVENT, {'code' : 2, 'rows' : int(self.rows.value), 'cols' : int(self.cols.value)}))
        self.close()

class SaveDialog(gui.Dialog):
    """ The dialog used for saving a file """
    def __init__(self, get_props, get_blocks, **params):
        self.get_props = get_props
        self.get_blocks = get_blocks
        title = gui.Label('Save Level')
        main = gui.Table()
        main.tr()

        e = gui.Label('Level Name:')
        self.i = gui.Input()
        main.td(e)
        main.td(self.i)

        main.tr()

        t = gui.Table(); t.tr()
        e = gui.Button('Save')
        e.connect(gui.CLICK, self.save, None)
        t.td(e)
        e = gui.Button('Cancel')
        e.connect(gui.CLICK, self.close, None)
        t.td(e)
        main.td(t, align=1, colspan=2)

        gui.Dialog.__init__(self, title, main)

    def save(self, value):
        """ Save level """
        dlg = StatusDialog('Saving...')
        self.close()
        dlg.open()
        save(self.get_props(), self.get_blocks(), self.i.value)
        dlg.close()
        dlg = Message('Saved', 'Finished saving level') # woop-de-do!
        dlg.open() # tell user we are done

class StatusDialog(gui.Dialog):
    """ A status message """
    def __init__(self, message, **params):
        title = gui.Label('')
        main = gui.Label(message)
        gui.Dialog.__init__(self, title, main)

class Message(gui.Dialog):
    """ A message with an ok button """
    def __init__(self, title, message, **params):
        title = gui.Label(title)
        main = gui.Table()
        main.tr()
        main.td(gui.Label(message))
        e = gui.Button('OK')
        e.connect(gui.CLICK, self.close, None)
        main.tr()
        main.td(e, align=0)
        gui.Dialog.__init__(self, title, main)

class PropertiesDialog(gui.Dialog):
    """ The dialog used for editing the properties of the level """
    def __init__(self, properties, **params):
        self.properties = properties
        title = gui.Label('Level Properties')
        main = gui.Table()
        main.tr()

        e = gui.Button('Background Color')
        e.connect(gui.CLICK, self.colorpick, None)
        main.td(e, colspan=3)

        main.tr()

        main.td(gui.Label('Music: '))
        self.music = gui.Input(self.properties['music'])
        main.td(self.music, colspan=2)

        main.tr()

        e = gui.Button('Ok')
        e.connect(gui.CLICK, self.finish, None)
        main.td(e)
        e = gui.Button('Cancel')
        e.connect(gui.CLICK, self.close, None)
        main.td(e)
        e = gui.Button('Browse')
        e.connect(gui.CLICK, self.browse, None)
        main.td(e)

        gui.Dialog.__init__(self, title, main)

    def browse(self, value):
        dlg = gui.FileDialog('Select Music File', 'Select', path=os.path.split(self.music.value)[0])
        dlg.connect(gui.CHANGE, self.browse_finish, dlg)
        dlg.open()

    def browse_finish(self, dlg):
        self.adjust_x('music', dlg.value)

    def colorpick(self, value):
        value = self.properties['background']
        col1 = hex(value[0])[2:]
        col2 = hex(value[1])[2:]
        col3 = hex(value[2])[2:]
        if len(col1) < 2: col1 = '0'+col1
        if len(col2) < 2: col2 = '0'+col2
        if len(col3) < 2: col3 = '0'+col3
        dlg = ColorDialog('#'+col1+col2+col3+'ff')
        dlg.connect(gui.CHANGE, self.colorpick_finish, dlg)
        dlg.open()

    def colorpick_finish(self, dlg):
        self.adjust_x('background', dlg.value[:3])

    def adjust(self, properties):
        self.properties = dict(properties)
        self.music.value = properties['music']

    def adjust_x(self, prop, value):
        self.properties[prop] = value
        self.adjust(self.properties)

    def adjust_open(self, properties):
        self.adjust(properties())
        self.open()

    def finish(self, v):
        pygame.event.post(pygame.event.Event(USEREVENT, {'code' : 4}))
        self.close()

def make_btn_back(screensize):
    backimg = pygame.image.load(fixpath('./data/images/misc/buttonback.png'))
    btnback = pygame.Surface((screensize[0], 25)); btnback.fill((255,255,255))
    if screensize[0] % 24 != 0: grid = make_grid((screensize[0]+24, 24), 24, 24)
    else: grid = make_grid((screensize[0], 24), 24, 24)
    for pos in grid: btnback.blit(backimg, (pos[0], 0))
    return btnback

def _quit(v):
    # can't have multiple arguments from menus
    pygame.event.post(pygame.event.Event(QUIT))

def level_editor(screen):
    def get_properties(): return properties
    def get_blocks(): return blocks
    pygame.display.set_caption('Mayze Level Editor')
    pygame.display.set_icon(pygame.image.load(fixpath('./data/images/icons/16x16.png')))
    screen.fill((127,127,127))
    pygame.mouse.set_visible(0)

    # setup everything
    pointer    = Pointer(0,0,'add')
    pointer.set_pos(pygame.mouse.get_pos())
    screensize = screen.get_size()
    grid       = make_grid((640,480), 32, 32)
    gridimg    = drawgrid((640,480), 32, 32, (0,0,0), transparent=False)
    gridpos    = gridimg.get_rect()
    staticback = pygame.Surface(gridimg.get_size())
    staticback.set_colorkey((255,255,255)); staticback.fill((255,255,255))
    blockset   = [] # list of positions where there is something
    buttonback = make_btn_back(screensize)
    rocks      = pygame.image.load(fixpath('./data/images/blocks/rocks.png'))
    player     = pygame.image.load(fixpath('./data/images/player/normal.png'))
    redball    = pygame.image.load(fixpath('./data/images/enemies/redball.png'))
    dialogs    = 0
    blocks     = [] # detailed list of objects and their positions
    properties = {'background' : (255,255,255), 'music' : '', 'size' : (640,480), 'playerstart' : None}

    # make gui
    toolapp     = gui.App()
    toolcont    = gui.Container(align=-1, valign=-1)
    toolbar     = gui.Table()
    toolbar.tr()
    newdialog   = NewDialog(None)
    newdialog.connect(gui.OPEN, function, pygame.event.post, pygame.event.Event(USEREVENT, {'code' : 0}))
    newdialog.connect(gui.CLOSE, function, pygame.event.post, pygame.event.Event(USEREVENT, {'code' : 1}))
    openfiledlg = gui.FileDialog('Load Level', 'Open', path=fixpath('./data/levels/'))
    openfiledlg.connect(gui.OPEN, function, pygame.event.post, pygame.event.Event(USEREVENT, {'code' : 0}))
    openfiledlg.connect(gui.CLOSE, function, pygame.event.post, pygame.event.Event(USEREVENT, {'code' : 1}))
    openfiledlg.connect(gui.CHANGE, function, pygame.event.post, pygame.event.Event(USEREVENT, {'code' : 3}))
    propdlg     = PropertiesDialog(properties)
    propdlg.connect(gui.OPEN, function, pygame.event.post, pygame.event.Event(USEREVENT, {'code' : 0}))
    propdlg.connect(gui.CLOSE, function, pygame.event.post, pygame.event.Event(USEREVENT, {'code' : 1}))
    savedlg     = SaveDialog(get_properties, get_blocks)
    savedlg.connect(gui.OPEN, function, pygame.event.post, pygame.event.Event(USEREVENT, {'code' : 0}))
    savedlg.connect(gui.CLOSE, function, pygame.event.post, pygame.event.Event(USEREVENT, {'code' : 1}))
    menu = gui.Menus([
        ('File/New',        newdialog.open,      None),
        ('File/Open',       openfiledlg.open,    None),
        ('File/Save',       savedlg.open,        None),
        ('File/Exit',       _quit,               None),
        ('Edit/Properties', propdlg.adjust_open, get_properties),
        ])
    menu.connect(gui.ENTER, function, pygame.event.post, pygame.event.Event(USEREVENT, {'code' : 0}))
    menu.connect(gui.EXIT, function, pygame.event.post, pygame.event.Event(USEREVENT, {'code' : 1}))
    toolbar.td(menu,colspan=2)
    tool        = gui.Select('rock')
    tool.add('Rock',    'rock')
    tool.add('RedBall', 'redball')
    tool.add('Player',  'player')
    toolbar.td(tool)
    toolcont.add(toolbar,0,0)
    toolapp.init(toolcont)

    # initialize
    clock = pygame.time.Clock()
    static = pygame.sprite.Group()
    pointer.update()
    screen.blit(pointer.image, pointer.rect)
    screen.blit(buttonback, (0,0))
    pygame.display.flip()

    while 1:
        clock.tick(30)

        mousegridpos = findingrid(pointer.x-gridpos[0], pointer.y-gridpos[1], grid)
        if mousegridpos is not None: mousegridpos = mousegridpos[:2]
        if len(toolapp.windows) < 1 and dialogs > 0: dialogs = 0

        for event in pygame.event.get():
            toolapp.event(event)
            if event.type == QUIT:
                return
            elif event.type == MOUSEBUTTONDOWN:
                if event.button == 1 and pointer.y > 25 and mousegridpos is not None and len(toolapp.windows) < 1:
                    if pointer.state == 'add':
                        if tool.value != 'player':
                            if tool.value == 'rock': static.add(Static(mousegridpos[0], mousegridpos[1], rocks, 'rock'))
                            elif tool.value == 'redball': static.add(Static(mousegridpos[0]+1, mousegridpos[1]+1, redball, 'redball'))
                            blockset.append(mousegridpos)
                            blocks.append((tool.value, mousegridpos))
                        else:
                            # delete an existing player (we want only one)
                            oldstart = []
                            for sprite in static.sprites():
                                if sprite.value == 'player':
                                    sprite.kill()
                                    oldstart.append(sprite.x); oldstart.append(sprite.y)
                                    blockset.remove((sprite.x-1, sprite.y-1))
                            _blocks = list(blocks) # don't iterate over mutating object
                            for block in _blocks:
                                if block[1] == oldstart:
                                    blocks.remove((block[0], oldstart))
                            static.add(Static(mousegridpos[0]+1, mousegridpos[1]+1, player, 'player'))
                            blockset.append(mousegridpos)
                            blocks.append(('player', mousegridpos))
                            properties['playerstart'] = mousegridpos
                    elif pointer.state == 'delete':
                        if tool.value == 'rock':
                            for sprite in static.sprites():
                                if sprite.x == mousegridpos[0] and\
                                   sprite.y == mousegridpos[1] and\
                                   sprite.value == tool.value:
                                    sprite.kill()
                                    blockset.remove(mousegridpos)
                        else:
                            for sprite in static.sprites():
                                if sprite.x == mousegridpos[0]+1 and\
                                   sprite.y == mousegridpos[1]+1 and\
                                   sprite.value == tool.value:
                                    sprite.kill()
                                    blockset.remove(mousegridpos)
                        _blocks = list(blocks) # don't iterate over mutating object
                        for block in _blocks:
                            if block[1] == mousegridpos:
                                blocks.remove((block[0], mousegridpos))
                        if tool.value == 'player': properties['playerstart'] = None # set to none is no player is present
            elif event.type == VIDEORESIZE:
                screen = pygame.display.set_mode(event.size, DOUBLEBUF|RESIZABLE)
                screensize = screen.get_size()
                buttonback = make_btn_back(screensize)

            elif event.type == USEREVENT:
                if event.code == 0: dialogs += 1 # dialog openned
                elif event.code == 1: dialogs -= 1 # dialog closed
                elif event.code == 2:
                    # new level dialog is done
                    static = pygame.sprite.Group()
                    blockset = []; blocks = []
                    grid = make_grid((int(event.cols)*32, int(event.rows)*32), 32,32)
                    gridimg = drawgrid((int(event.cols)*32, int(event.rows)*32), 32,32, (0,0,0), transparent=False)
                    x = gridpos.x; y = gridpos.y
                    gridpos = gridimg.get_rect()
                    gridpos.x = x; gridpos.y = y
                    properties['size'] = (int(event.cols)*32, int(event.rows)*32)
                    del x, y
                elif event.code == 3:
                    # load level dialog is done
                    if openfiledlg.value:
                        level = {}; blockset = []; blocks=[]; static = pygame.sprite.Group()
                        execfile(openfiledlg.value, level)
                        grid = make_grid(level['size'], 32, 32)
                        if level['background'][0]+level['background'][1]+level['background'][2] <= 381:
                            gridimg = drawgrid((level['size'][0], level['size'][1]), 32,32, (255,255,255), level['background'], transparent=False)
                        else:
                            gridimg = drawgrid((level['size'][0], level['size'][1]), 32,32, (0,0,0), level['background'], transparent=False)
                        for block in level['blocks']:
                            if block.type == 'rocks':
                                blockset.append((block.x, block.y))
                                static.add(Static(block.x, block.y, rocks))
                                blocks.append(('rock', (block.x, block.y)))
                        for enemy in level['enemies']:
                            if enemy.type == 'redball':
                                blockset.append((enemy.x, enemy.y))
                                static.add(Static(enemy.x+1, enemy.y+1, redball))
                                blocks.append(('redball', (enemy.x, enemy.y)))
                        static.add(Static(level['playerstart'][0]+1, level['playerstart'][1]+1, player, 'player'))
                        blockset.append(level['playerstart'])
                        blocks.append(('player', level['playerstart']))
                        properties = {'background' : tuple(level['background']), 'size' : tuple(level['size']), 'music' : level['music'], 'playerstart' : level['playerstart']}
                        del level, block
                elif event.code == 4:
                    # properties dialog closed with ok
                    properties = dict(propdlg.properties)
                    if properties['background'][0]+properties['background'][1]+properties['background'][2] <= 381:
                        gridimg = drawgrid((gridimg.get_width(), gridimg.get_height()), 32,32, (255,255,255), properties['background'], transparent=False)
                    else:
                        gridimg = drawgrid((gridimg.get_width(), gridimg.get_height()), 32,32, (0,0,0), properties['background'], transparent=False)

        if pygame.mouse.get_focused() and len(toolapp.windows) < 1:
            if pointer.x < 32 and pointer.y < 57 and pointer.y > 25:
                if pointer.state != 'upleft': pointer.set_state('upleft')
                gridpos.x += 3; gridpos.y += 3
            elif pointer.x < 32 and pointer.y > screensize[1]-32:
                if pointer.state != 'downleft': pointer.set_state('downleft')
                gridpos.x += 3; gridpos.y -= 3
            elif pointer.x > screensize[0]-32 and pointer.y > screensize[1]-32:
                if pointer.state != 'downright': pointer.set_state('downright')
                gridpos.x -= 3; gridpos.y -= 3
            elif pointer.x > screensize[0]-32 and pointer.y < 57 and pointer.y > 25:
                if pointer.state != 'upright': pointer.set_state('upright')
                gridpos.x -= 3; gridpos.y += 3
            elif pointer.x < 32 and pointer.y > 57 and pointer.y < screensize[1]-32:
                if pointer.state != 'left': pointer.set_state('left')
                gridpos.x += 3
            elif pointer.x > screensize[0]-32 and pointer.y > 57 and pointer.y < screensize[1]-32:
                if pointer.state != 'right': pointer.set_state('right')
                gridpos.x -= 3
            elif pointer.y < 57 and pointer.y > 25 and pointer.x > 32 and pointer.x < screensize[0]-32:
                if pointer.state != 'up': pointer.set_state('up')
                gridpos.y += 3
            elif pointer.y > screensize[1]-32 and pointer.x > 32 and pointer.x < screensize[0]-32:
                if pointer.state != 'down': pointer.set_state('down')
                gridpos.y -= 3
            elif pointer.x > 32 and pointer.x < screensize[0]-32 and\
                 pointer.y > 57 and pointer.y < screensize[1]-32:
                if mousegridpos is None and pointer.state != 'add': pointer.set_state('add')
                elif mousegridpos not in blockset and pointer.state != 'add': pointer.set_state('add')
                elif mousegridpos in blockset and pointer.state != 'delete': pointer.set_state('delete')
            elif pointer.y < 26:
                if pointer.state != 'add': pointer.set_state('add')

            

        pointer.set_pos(pygame.mouse.get_pos())

        pointer.update()
        screen.fill((127,127,127))
        staticback.fill((255,255,255))
        static.draw(staticback)
        screen.blit(gridimg, gridpos)
        screen.blit(staticback, gridpos)
        screen.blit(buttonback, (0,0))
        toolapp.paint(screen)
        screen.blit(pointer.image, pointer.rect)
        pygame.display.flip()

def standalone():
    pygame.init()
    screen = pygame.display.set_mode((640,480), DOUBLEBUF|RESIZABLE)
    level_editor(screen)
    pygame.quit()
    return

if __name__ == '__main__':
    try: import psyco
    except ImportError: pass
    else: psyco.full()
    standalone()
