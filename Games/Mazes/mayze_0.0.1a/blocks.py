##This file is part of Mayze.
##
##Mayze is free software: you can redistribute it and/or modify
##it under the terms of the GNU General Public License as published by
##the Free Software Foundation, either version 3 of the License, or
##(at your option) any later version.
##
##Mayze is distributed in the hope that it will be useful,
##but WITHOUT ANY WARRANTY; without even the implied warranty of
##MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##GNU General Public License for more details.
##
##You should have received a copy of the GNU General Public License
##along with Mayze.  If not, see <http://www.gnu.org/licenses/>.
import pygame

class Block(pygame.sprite.Sprite):
    """ A solid block. """
    def __init__(self, x, y, image, block_type, collidelist=None):
        pygame.sprite.Sprite.__init__(self)
        self.image = image
        self.rect = image.get_rect()
        self.x = x; self.y = y
        self.type = block_type
        self.collidelist = collidelist
        self.active = True # if the block is too far from the player, collisions are not detected.

    def update(self):
        self.rect.x = self.x
        self.rect.y = self.y
