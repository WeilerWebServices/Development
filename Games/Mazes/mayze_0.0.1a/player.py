##This file is part of Mayze.
##
##Mayze is free software: you can redistribute it and/or modify
##it under the terms of the GNU General Public License as published by
##the Free Software Foundation, either version 3 of the License, or
##(at your option) any later version.
##
##Mayze is distributed in the hope that it will be useful,
##but WITHOUT ANY WARRANTY; without even the implied warranty of
##MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##GNU General Public License for more details.
##
##You should have received a copy of the GNU General Public License
##along with Mayze.  If not, see <http://www.gnu.org/licenses/>.
import pygame

class Player(pygame.sprite.Sprite):
    def __init__(self, pos, collisions):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.image.load('./data/images/player/normal.png')
        self.image.set_colorkey(self.image.get_at((0,0)))
        self.rect = self.image.get_rect()
        self.x = pos[0]; self.y = pos[1]
        self.collisions = collisions
        self.type = 'player'
        self.facing = 'up'
        self.speed = 5
        self.lives = 3
        self.invincible = False
        self.dead = False
        self.paused = False

    def left(self):
        self.facing = 'left'
        self.x -= self.speed

    def up(self):
        self.facing = 'up'
        self.y -= self.speed

    def down(self):
        self.facing = 'down'
        self.y += self.speed

    def right(self):
        self.facing = 'right'
        self.x += self.speed

    def pause(self):
        self.paused = not self.paused

    def collide(self, obj):
        if self.collisions.has_key(obj.type):
            if self.collisions[obj.type] == 'solid':
                # where are we in comparison to this object?
                if obj.rect.collidepoint(self.rect.topleft):
                    # upper left corner is in
                    if obj.rect.collidepoint(self.rect.topright):
                        # top edge is in, move down by overlap
                        self.y = obj.rect.bottom
                    elif obj.rect.collidepoint(self.rect.bottomleft):
                        # left edge is in, move right by overlap
                        self.x = obj.rect.right
                    else:
                        # no edge in. (at least on this side)
                        # find the overlap for x and y
                        ovx = abs(self.x-obj.rect.right)
                        ovy = abs(self.y-obj.rect.bottom)
                        if ovx >= ovy: # came from bottom
                            self.y = obj.rect.bottom
                        else: # came from right
                            self.x = obj.rect.right
                elif obj.rect.collidepoint(self.rect.topright):
                    # upper right corner is in
                    if obj.rect.collidepoint(self.rect.bottomright):
                        # right edge is in, move left by overlap
                        self.x = obj.rect.left-self.rect.width
                    else:
                        # no edge is in. (at least on this side)
                        # find the overlap for x and y
                        ovx = abs(self.rect.right-obj.rect.left)
                        ovy = abs(self.y-obj.rect.bottom)
                        if ovx >= ovy: # came from bottom
                            self.y = obj.rect.bottom
                        else: # came from left
                            self.x = obj.rect.left-self.rect.width
                elif obj.rect.collidepoint(self.rect.bottomright):
                    # bottom right corner is in
                    if obj.rect.collidepoint(self.rect.bottomleft):
                        # bottom edge is in
                        self.y = obj.rect.top-self.rect.height
                    else:
                        # no edge is in. (at least on this side)
                        # find the overlap for x and y
                        ovx = abs(self.x-obj.rect.left-self.rect.width)
                        ovy = abs(self.y-obj.rect.top-self.rect.height)
                        if ovx <= ovy: # came from top
                            self.y = obj.rect.top-self.rect.height
                        else: # came from left
                            self.x = obj.rect.left-self.rect.width
                elif obj.rect.collidepoint(self.rect.bottomleft):
                    # bottom left corner is in
                    # we have eliminiated all sides
                    # find the overlap for x and y
                    ovx = abs(self.x-obj.rect.right)
                    ovy = abs(self.rect.bottom-obj.rect.top)
                    if ovx >= ovy: # came from top
                        self.y = obj.rect.top-self.rect.height
                    elif ovx < ovy: # came from right
                        self.x = obj.rect.right
                        
            if self.collisions[obj.type] == 'kill' and not self.invincible:
                self.lives -= 1

    def update(self, blocks, enemies):
        if not self.paused:
            self.rect.x = self.x
            self.rect.y = self.y
            
            for block in blocks.sprites():
                if abs(self.x-block.x) < 40 and\
                   abs(self.y-block.y) < 40:
                    if self.rect.colliderect(block.rect):
                        self.collide(block)
            for enemy in enemies.sprites():
                if abs(self.x-enemy.x) < 40 and\
                   abs(self.y-enemy.y) < 40:
                    if self.rect.colliderect(enemy.rect):
                        self.collide(enemy)
            
            self.rect.x = self.x
            self.rect.y = self.y
