##This file is part of Mayze.
##
##Mayze is free software: you can redistribute it and/or modify
##it under the terms of the GNU General Public License as published by
##the Free Software Foundation, either version 3 of the License, or
##(at your option) any later version.
##
##Mayze is distributed in the hope that it will be useful,
##but WITHOUT ANY WARRANTY; without even the implied warranty of
##MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##GNU General Public License for more details.
##
##You should have received a copy of the GNU General Public License
##along with Mayze.  If not, see <http://www.gnu.org/licenses/>.
import pygame, os

# Universal function, put first

def fixpath(path, normal=True):
    newpath = ''
    if len(path.split('\\')) == 1 and os.sep != '/':
        for i, word in enumerate(path.split('/')):
            if i and normal: newpath += '\\'+word
            if i and not normal: newpath += '\\\\'+word
            if not i: newpath += word
    else:
        for i, word in enumerate(path.split('\\')):
            if i: newpath += word+'/'
            if not i: newpath += word
    return newpath

# Constants

LEVEL_OVERLAY = """# generated by level maker 1.0
from blocks import *
from enemies import *
from functions import *
import pygame
%s

size = %s
grid = make_grid(size, 32, 32)
playerstart = %s
screenstart = %s
background = %s
music = %s
blocks = pygame.sprite.Group()
enemies = pygame.sprite.Group()
collidedict = {'rocks' : 'solid', 'redball' : 'kill'}
bulletdict = {'redball' : 'kill'}

%s"""

BLOCK_OVERLAY = """bpos = %s
bimg = pygame.image.load('"""+fixpath('./data/images/blocks/rocks.png', False)+"""')
for pos in bpos:
    blocks.add(Block(pos[0], pos[1], bimg, 'rocks'))"""

ENEMY_OVERLAY = """epos = %s
for pos in epos:
    enemies.add(%s(pos, {'rocks' : 'solid'}, size))"""

# functions

def get_levels():
    levels = []
    for f in os.listdir(fixpath('./data/levels/')):
        if len(f.split('.')) > 1 and f.split('.')[1] == 'py':
            levels.append(f.split('.')[0])
    return levels

def make_grid_old(screensize, xinterval, yinterval, xstart=0, ystart=0):
    grid = []
    for x in range(xstart, screensize[0], xinterval):
        toadd = []
        for y in range(ystart, screensize[1], yinterval):
            toadd.append((x, y, x+xinterval, y+yinterval))
        grid.append(toadd)
    return grid

def make_grid(size, xinterval, yinterval, xstart=0, ystart=0):
    grid = []
    for x in range(xstart, size[0], xinterval):
        for y in range(ystart, size[1], yinterval):
            grid.append((x, y, x+xinterval, y+yinterval))
    return grid

def findingrid_old(x, y, grid):
    for column in grid:
        for pos in column:
            if x >= pos[0] and x <= pos[2] and\
               y >= pos[1] and y <= pos[3]:
                return pos

def findingrid(x, y, grid):
    for pos in grid:
        if x >= pos[0] and x <= pos[2] and\
           y >= pos[1] and y <= pos[3]:
            return pos

def enumingrid_old(x, y, grid):
    for i, column in enumerate(grid):
        for i2, pos in enumerate(column):
            if x >= pos[0] and x <= pos[2] and\
               y >= pos[1] and y <= pos[3]:
                return (i, i2)

def enumingrid(x, y, grid):
    for i, pos in enumerate(grid):
        if x >= pos[0] and x <= pos[2] and\
           y >= pos[1] and y <= pos[3]:
            return i

def drawgrid(screensize, xinterval, yinterval, color, bgcolor=None, transparent=True):
    gridimg = pygame.Surface(screensize)
    if not bgcolor:
        if color == (255,255,255): bgcolor = (0,0,0)
        else: bgcolor = (255,255,255)
        gridimg.fill(bgcolor)
    else: gridimg.fill(bgcolor)
    for x in range(0, screensize[0], xinterval):
        pygame.draw.line(gridimg, color, (x, 0), (x, screensize[1]))
    for y in range(0, screensize[1], yinterval):
        pygame.draw.line(gridimg, color, (0, y), (screensize[0], y))
    if transparent: gridimg.set_colorkey(bgcolor)
    return gridimg

def save(properties, blocks, level_name):
    """ Save level """
    bpos = []
    rpos = []
    for block in blocks:
        if block[0] == 'rock':
            bpos.append(block[1])
        if block[0] == 'redball':
            rpos.append(block[1])
    if properties['music']: music = properties['music']
    else: music = None
    if properties['playerstart']: playerstart = properties['playerstart']
    else: playerstart = (0,0)
    level_info = (
        '', # extra imports
        str(properties['size']), # level size
        str(playerstart), # player start position
        str((properties['size'][0]/2,properties['size'][1]/2)), # screen start position
        str(properties['background']), # background color
        str(music), # music
        BLOCK_OVERLAY % str(bpos) +'\n\n'+ ENEMY_OVERLAY % (str(rpos), # enemies
                                                     'RedBall') # object name
        )
    level = LEVEL_OVERLAY % level_info # generate level
    path = fixpath('./data/levels/%s.py') % level_name
    open(path, 'w').write(level)

def load_level(level_name):
    level = {}
    execfile(fixpath('./data/levels/%s.py') % level_name, level)
    return level

def function(callback, *args, **kwargs):
    # anything built-in or in c(++) doesn't have a func_code. PGU requires one.
    return callback(*args, **kwargs)
