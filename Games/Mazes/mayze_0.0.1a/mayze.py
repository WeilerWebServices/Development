##This file is part of Mayze.
##
##Mayze is free software: you can redistribute it and/or modify
##it under the terms of the GNU General Public License as published by
##the Free Software Foundation, either version 3 of the License, or
##(at your option) any later version.
##
##Mayze is distributed in the hope that it will be useful,
##but WITHOUT ANY WARRANTY; without even the implied warranty of
##MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##GNU General Public License for more details.
##
##You should have received a copy of the GNU General Public License
##along with Mayze.  If not, see <http://www.gnu.org/licenses/>.
from pygame.locals import *
from pgu import gui
import pygame, time, os
from player import Player
from bullets import Bullet
from functions import *

def game(screen, level_name):
    pygame.mouse.set_visible(1)
    pygame.display.set_caption('Mayze')
    screen.fill((0,0,0))

    shootdelay = 10; shootcount = 0; won = False
    level = load_level(level_name)
    background = pygame.Surface(level['size'])
    background.fill(level['background'])
    player = Player(level['playerstart'], level['collidedict'])
    blocks = level['blocks']
    enemies = level['enemies']
    bullets = pygame.sprite.Group()
    font = pygame.font.Font(fixpath('./data/fonts/freesansbold.ttf'), 12)
    gameoverfont = winfont = pygame.font.Font(fixpath('./data/fonts/freesansbold.ttf'), 72)
    gameovertext = gameoverfont.render('Game Over', 1, (255,0,0))
    gameoverrect = gameovertext.get_rect()
    gameoverrect.centerx = 320; gameoverrect.centery = 240
    wintext = winfont.render('You Win', 1,  (255,0,0))
    winrect = wintext.get_rect()
    winrect.centerx = 320; winrect.centery = 240
    startime = font.render('Time: 0', 1, (255,255,255))
    startrect = startime.get_rect()
    startrect.x = 0; startrect.y = 0
    invinciblecount = 0; invincibledelay = 90; invincibleon = False
    gameovercount = 0; gameoverdelay = 90
    wincount = 0; windelay = 90
    lives = int(player.lives); livestext = font.render('Lives: %s' % str(player.lives), 1, (255,255,255), (0,0,0))
    enemy_c = len(enemies.sprites()); enemy_text = font.render('Enemies: %s' % str(len(enemies.sprites())), 1, (255,255,255), (0,0,0))
    livesrect = livestext.get_rect()
    livesrect.x = screen.get_width()-livesrect.width
    livesrect.y = 0
    enemyrect = enemy_text.get_rect()
    enemyrect.centerx = screen.get_width()/2
    enemyrect.y     = 0

    clock = pygame.time.Clock()
    blocks.update()
    enemies.update(blocks, player)
    bullets.update(blocks, enemies)
    player.update(blocks, enemies)
    blocks.draw(background)
    enemies.draw(background)
    bullets.draw(background)
    starttime = time.time()
    background.blit(player.image, level['screenstart'])
    screen.blit(background, (level['screenstart'][0]-player.x, level['screenstart'][1]-player.y))
    screen.blit(startime, startrect)
    screen.blit(livestext, livesrect)

    while 1:
        clock.tick(30)

        for event in pygame.event.get():
            if event.type == QUIT:
                return

        if shootcount > 0: shootcount -= 1
        if invinciblecount > 0: invinciblecount -= 1
        if player.invincible: invincibleon = not invincibleon
        if invinciblecount == 0: player.invincible = False
        if gameovercount > 0: gameovercount -= 1
        if gameovercount == 0 and player.dead: return
        if wincount > 0: wincount -= 1
        if wincount == 0 and won: return

        if lives > player.lives and player.lives >= 0:
            lives = player.lives
            livestext = font.render('Lives: %s' % str(player.lives), 1, (255,255,255), (0,0,0))
            player.invincible = True
            invinciblecount = invincibledelay

        if enemy_c != len(enemies.sprites()):
            enemy_c = len(enemies.sprites())
            enemy_text = font.render('Enemies: %s' % str(len(enemies.sprites())), 1, (255,255,255), (0,0,0))
            enemyrect = enemy_text.get_rect()
            enemyrect.centerx = screen.get_width()/2
            enemyrect.y     = 0

        if player.lives == 0 and not player.dead:
            gameovercount = gameoverdelay
            player.dead = True

        if len(enemies.sprites()) == 0 and not won:
            wincount = windelay
            won = True
            
        keys = pygame.key.get_pressed()
        if not player.dead and not won:
            if keys[K_LEFT]: player.left()
            if keys[K_UP]: player.up()
            if keys[K_RIGHT]: player.right()
            if keys[K_DOWN]: player.down()
            if keys[K_SPACE]:
                if shootcount == 0:
                    if player.facing == 'up':
                        posx = player.rect.centerx
                        posy = player.y - 1
                        dx = 0; dy = -10
                    elif player.facing == 'left':
                        posx = player.x - 1
                        posy = player.rect.centery
                        dx = -10; dy = 0
                    elif player.facing == 'right':
                        posx = player.rect.right + 1
                        posy = player.rect.centery
                        dx = 10; dy = 0
                    elif player.facing == 'down':
                        posx = player.rect.centerx
                        posy = player.rect.bottom + 1
                        dx = 0; dy = 10
                    b = Bullet(posx, posy, dx, dy, level['bulletdict'])
                    bullets.add(b)
                    shootcount = shootdelay

        if player.x < 0: player.x = 0
        if player.x > level['size'][0]-player.rect.width: player.x = level['size'][0]-player.rect.width
        if player.y < 0: player.y = 0
        if player.y > level['size'][1]-player.rect.height: player.y = level['size'][1]-player.rect.height

        screen.fill((0,0,0))
        background.fill(level['background'])
        player.update(blocks, enemies)
        enemies.update(blocks, player)
        bullets.update(blocks, enemies)
        player.rect.x = player.x; player.rect.y = player.y
        if not player.invincible and not player.dead and not won: background.blit(player.image, player.rect)
        if player.invincible and invincibleon and not player.dead: background.blit(player.image, player.rect)
        blocks.draw(background)
        enemies.draw(background)
        bullets.draw(background)
        startime = font.render('Time: '+str(int(round(abs(starttime-time.time())))), 1, (255,255,255), (0,0,0))
        screen.blit(background, (level['screenstart'][0]-player.x, level['screenstart'][1]-player.y))        
        screen.blit(startime, startrect)
        screen.blit(livestext, livesrect)
        screen.blit(enemy_text, enemyrect)
        if gameovercount: screen.blit(gameovertext, gameoverrect)
        if wincount: screen.blit(wintext, winrect)
        pygame.display.flip()

def main():
    pygame.init()
    screen = pygame.display.set_mode((640,480), DOUBLEBUF)
    app = gui.App()
    con = gui.Container()
    dlg = gui.FileDialog('Load Level', 'Open', path=fixpath('./data/levels/'))
    dlg.connect(gui.CHANGE, function, pygame.event.post, pygame.event.Event(USEREVENT, {'code' : 0}))
    app.init(con)
    dlg.open()
    screen.fill((255,255,255))
    app.paint(screen)
    clock = pygame.time.Clock()
    pygame.display.flip()

    while 1:
        clock.tick(30)

        for event in pygame.event.get():
            app.event(event)
            if event.type == QUIT:
                return
            if event.type == USEREVENT:
                if event.code == 0 and dlg.value:
                    # get level name and start gameplay
                    game(screen, dlg.value.split(os.sep)[-1].split('.')[0])
                    # re-open, load other level
                    dlg.open()

        screen.fill((255,255,255))
        app.paint(screen)
        pygame.display.flip()

if __name__ == '__main__': main()
