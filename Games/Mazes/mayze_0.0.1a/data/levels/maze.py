# mayze blank level
from blocks import Block
from enemies import RedBall
from functions import make_grid_old, findingrid_old
import pygame, random

size = (640,480)
grid = make_grid_old(size, 32, 32)
playerstart = (0,0)
screenstart = (320,240)
background = (255,255,255)
music = None
blocks = pygame.sprite.Group()
enemies = pygame.sprite.Group()
collidedict = {'rocks' : 'solid', 'redball' : 'kill'}
bulletdict = {'redball' : 'kill'}

bimg = pygame.image.load('./data/images/blocks/rocks.png')
rbimg = pygame.image.load('./data/images/enemies/redball.png')
rbimg.set_colorkey(rbimg.get_at((0,0)))
bpos = [grid[0][1], grid[0][9],
        grid[1][1], grid[1][3], grid[1][4], grid[1][5], grid[1][6], grid[1][7], grid[1][9],
        grid[2][5], grid[2][9],
        grid[3][0], grid[3][1], grid[3][2], grid[3][3], grid[3][5], grid[3][7], grid[3][8], grid[3][9], grid[3][10], grid[3][12], grid[3][13], grid[3][14],
        grid[4][5], grid[4][7], grid[4][10], grid[4][12],
        grid[5][1], grid[5][3], grid[5][4], grid[5][5], grid[5][7], grid[5][10],
        grid[6][1], grid[6][5], grid[6][7], grid[6][10], grid[6][11], grid[6][12], grid[6][13],
        grid[7][1], grid[7][2], grid[7][3], grid[7][5], grid[7][6], grid[7][7], grid[7][10],
        grid[8][1], grid[8][3], grid[8][4], grid[8][5], grid[8][10], grid[8][12], grid[8][13], grid[8][14],
        grid[9][1], grid[9][3], grid[9][7], grid[9][8], grid[9][9], grid[9][10], grid[9][12],
        grid[10][1], grid[10][3], grid[10][5], grid[10][12],
        grid[11][1], grid[11][3], grid[11][5], grid[11][7], grid[11][8], grid[11][9], grid[11][10], grid[11][11], grid[11][12],
        grid[12][1], grid[12][5], grid[12][7], grid[12][12],
        grid[13][1], grid[13][3], grid[13][5], grid[13][7], grid[13][10], grid[13][12],
        grid[14][3], grid[14][5], grid[14][7], grid[14][8], grid[14][9], grid[14][10], grid[14][12],
        grid[15][0], grid[15][1], grid[15][2], grid[15][3], grid[15][4], grid[15][5], grid[15][7], grid[15][12],
        grid[16][5], grid[16][9], grid[16][10], grid[16][11], grid[16][12],
        grid[17][5], grid[17][6], grid[17][7], grid[17][12],
        grid[18][7], grid[18][8], grid[18][10], grid[18][11], grid[18][12], grid[18][13],
        grid[19][5]]
for pos in bpos:
    blocks.add(Block(pos[0], pos[1], bimg, 'rocks'))
for i in range(10):
    pos = random.choice(random.choice(grid))
    while pos in bpos: pos = random.choice(random.choice(grid))
    enemies.add(RedBall(pos, {'rocks' : 'solid'}, size))
del bimg, pos, bpos, i, rbimg
