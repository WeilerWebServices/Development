﻿using System;
using System.ComponentModel.DataAnnotations;

namespace What_s_Happening_Lancaster.Models
{
    public class Item
    {
        public string Id { get; set; }

        [Required]
        public string Text { get; set; }

        [Required]
        public string Description { get; set; }
    }
}
